export function draw_graph(dot) {
  d3.select("#canvas").graphviz()
  .renderDot(dot);
}

export function draw_shortest_path(json) {
  $("#shortest-result").html("");
  let obj = JSON.parse(json);
  obj.weights.forEach((tup, _i, _a) => {
    let name, weight;
    [name, weight] = tup;
    $("#shortest-result").html($("#shortest-result").html() + "<p> The shortest path to " + name + " is " + weight + "<\p>");
  });
  $("#shortest-result").html($("#shortest-result").html() + '<div id="mintree">&nbsp;</div>');
  d3.select("#mintree").graphviz()
    .renderDot(obj.minimal_tree);
}
