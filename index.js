const rust = import('./graphy');

function getNodes() {
  return $("#nodes").val();
}

function getEdges() {
  return $("#edges").val();
}

function getDirectional() {
  return $("#directional:checked").val()?true:false
}

function getWeighted() {
  return $("#weighted:checked").val()?true:false
}

rust.then(wasm => {
  $("#draw_btn").click(() => { wasm.request_graph_draw(getNodes(), getEdges(), getDirectional(), getWeighted()); });
  $("#shortest_btn").click(() => { wasm.shortest_path(getNodes(), getEdges(), $("#shortest-from").val()); });
  wasm.request_graph_draw(getNodes(), getEdges(), getDirectional(), getWeighted());
  wasm.shortest_path(getNodes(), getEdges(), $("#shortest-from").val());
});

function getFromStorage(name) {
  if (typeof(Storage) !== "undefined") {
    let data = window.localStorage.getItem(name);
    if (data != null) {
      console.log(data);
      $("#" + name).val(data);
    }
  }
}

function getCheckboxFromStorage(name) {
  if (typeof(Storage) !== "undefined") {
    $("#" + name).prop("checked", window.localStorage.getItem(name) == 'true');
  }
}

function saveCheckboxToStorage(name) {
    if (typeof(Storage) !== "undefined") {
      $("#" + name).change(() => {
        window.localStorage.setItem(name, $("#" + name).prop("checked"));
    });
  }
}

function saveToStorage(name) {
    if (typeof(Storage) !== "undefined") {
      $("#" + name).change(() => {
        window.localStorage.setItem(name, $("#" + name).val());
    });
  }
}

function toggleShortest() {
    if (getWeighted() && getDirectional()) {
      $("#shortest-container").show();
      $("#shortest-result").show();
    } else {
      $("#shortest-container").hide();
      $("#shortest-result").hide();
    }
}

$(document).ready(() => {
  getFromStorage("nodes");
  saveToStorage("nodes");
  getFromStorage("edges");
  saveToStorage("edges");
  getCheckboxFromStorage("directional");
  saveCheckboxToStorage("directional");
  getCheckboxFromStorage("weighted");
  saveCheckboxToStorage("weighted");
  saveToStorage("shortest-from");
  getFromStorage("shortest-from");

  $("#weighted").click(() => {
    toggleShortest();
  });

  $("#directional").click(() => {
    toggleShortest();
  });
  toggleShortest();
});
