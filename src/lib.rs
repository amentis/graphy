#![feature(box_syntax)]
extern crate wasm_bindgen;
extern crate web_sys;
extern crate petgraph;
#[macro_use]
extern crate json;

use wasm_bindgen::prelude::*;
use petgraph::prelude::*;
use petgraph::dot::*;
use petgraph::algo;

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
    #[wasm_bindgen(js_namespace = console, js_name = log)]
    fn log_u32(a: u32);
    #[wasm_bindgen(js_namespace = console, js_name = log)]
    fn log_many(json: &str);
}

#[wasm_bindgen(module = "./draw")]
extern "C" {
    fn draw_graph(dot: &str);
    fn draw_shortest_path(json: &str);
}

macro_rules! console_log {
    ($($t:tt)*) => (log(&format_args!($($t)*).to_string()))
}

#[wasm_bindgen]
pub fn request_graph_draw(nodes: &str, edges: &str, directed : bool, weighted: bool) {
    console_log!("Initializing graph");
    let res = if directed { build_directed_graph(nodes, edges, weighted) }
    else { build_undirected_graph(nodes, edges, weighted) };
    if let Some(res) = res {
        draw_graph(res.as_str());
    }
}

#[wasm_bindgen]
pub fn shortest_path(nodes: &str, edges: &str, source: usize) {
    let mut graph = Graph::<&str, f32>::new();
    //adding nodes
    for line in nodes.lines() {
        graph.add_node(line);
    }

    //adding edges
    for edge in edges.lines() {
        let edge_split_vec = edge.splitn(3, ",").map(|s| s.trim()).collect::<Vec<_>>();
        let (from, to, weight_opt) : (usize, usize, Option<f32>) = match edge_split_vec.as_slice() {
            &[from_str, to_str, weight] => (from_str.parse().unwrap(), to_str.parse().unwrap(), Some(weight.parse().unwrap())),
            _ => {
                console_log!("Invalid edge: {}", edge);
                return;
            }
        };
        let weight = weight_opt.unwrap_or_default();
        graph.add_edge(NodeIndex::new(from), NodeIndex::new(to), weight);
    }
    let (weights, path) = algo::bellman_ford(&graph, NodeIndex::new(source)).unwrap();
    let weights = weights.into_iter().zip(graph.raw_nodes()).map(|(weight, node)| vec![node.weight.to_owned(), weight.to_string()]).collect::<Vec<_>>();

    let mut minimal_tree = Graph::new();
    for line in nodes.lines() {
        minimal_tree.add_node(line);
    }
    for (to, from) in path.into_iter().enumerate().filter(|(_, node)| node.is_some()) {
        let from = from.unwrap();
        let to = NodeIndex::new(to);
        minimal_tree.add_edge(from, to, graph.edge_weight(graph.find_edge(from, to).unwrap()).unwrap());
    }

    let dot = Dot::with_config(&minimal_tree, &[Config::EdgeIndexLabel]).to_string();

    let json = object!{
        "weights" => weights,
        "minimal_tree" => dot
    }.to_string();
    draw_shortest_path(json.as_str());
}

#[inline]
fn build_directed_graph(nodes: &str, edges: &str, weighted: bool) -> Option<String> {
    let mut graph = Graph::<&str, i32>::new();
    //adding nodes
    for line in nodes.lines() {
        graph.add_node(line);
    }

    //adding edges
    for edge in edges.lines() {
        let edge_split_vec = edge.splitn(3, ",").map(|s| s.trim()).collect::<Vec<_>>();
        let (from, to, weight_opt) : (usize, usize, Option<i32>) = match edge_split_vec.as_slice() {
            &[from_str, to_str] => (from_str.parse().unwrap(), to_str.parse().unwrap(), None),
            &[from_str, to_str, weight] => (from_str.parse().unwrap(), to_str.parse().unwrap(), Some(weight.parse().unwrap())),
            _ => {
                console_log!("Invalid edge: {}", edge);
                return None;
            }
        };
        if weight_opt.is_some() != weighted {
            console_log!("Invalid edge: {}", edge);
            return None;
        }
        let weight = weight_opt.unwrap_or_default();
        graph.add_edge(NodeIndex::new(from), NodeIndex::new(to), weight);
    }
    Some(Dot::with_config(&graph, &[if weighted { Config::EdgeIndexLabel } else { Config::EdgeNoLabel }]).to_string())
}


#[inline]
fn build_undirected_graph(nodes: &str, edges: &str, weighted: bool) -> Option<String> {
    let mut graph = Graph::new_undirected();
    //adding nodes
    for line in nodes.lines() {
        graph.add_node(line);
    }

    //adding edges
    for edge in edges.lines() {
        let edge_split_vec = edge.splitn(3, ",").map(|s|s.trim()).collect::<Vec<_>>();
        let (from, to, weight_opt) : (usize, usize, Option<i32>) = match edge_split_vec.as_slice() {
            &[from_str, to_str] => (from_str.parse().unwrap(), to_str.parse().unwrap(), None),
            &[from_str, to_str, weight] => (from_str.parse().unwrap(), to_str.parse().unwrap(), Some(weight.parse().unwrap())),
            _ => {
                console_log!("Invalid edge: {}", edge);
                return None;
            }
        };
        if weight_opt.is_some() != weighted {
            console_log!("Invalid edge: {}", edge);
            return None;
        }
        let weight = weight_opt.unwrap_or_default();
        graph.add_edge(NodeIndex::new(from), NodeIndex::new(to), weight);
    }
    Some(Dot::with_config(&graph, &[if weighted { Config::EdgeIndexLabel } else { Config::EdgeNoLabel }]).to_string())
}
