const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	devServer: {
		host: "0.0.0.0",
		port: 80
	},
	entry: './index.js',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'index.js',
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: "index.html"
		})
	],
	mode: 'development'
};
